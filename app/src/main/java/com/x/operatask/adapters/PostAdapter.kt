package com.x.operatask.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.x.operatask.R
import com.x.operatask.models.Post
import kotlinx.android.synthetic.main.post_item.view.*
import android.content.Intent
import android.net.Uri


class PostAdapter(val mContext : Context, var mPosts : MutableList<Post>) : RecyclerView.Adapter<PostAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(mContext)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView = layoutInflater.inflate(R.layout.post_item, parent, false)
        return ViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return mPosts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val post = mPosts[position]
        holder.bind(post)
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        fun bind(post: Post) {
            itemView.titleLabel.text = post.data.title
            itemView.subRedditLabel.text = post.data.subreddit_name_prefixed
            itemView.scoreLabel.text = "+ ${post.data.score}"

            itemView.setOnClickListener {
                openlink(itemView.context, post.data.permalink)
            }
        }

        private fun openlink(context: Context, permalink: String) {
            val url = "https://www.reddit.com$permalink"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            context.startActivity(i)
        }
    }
}