package com.x.operatask.models

data class Post(
        var data : PostData
)

data class PostData (
        var id : String,
        var title : String,
        var permalink : String,
        var subreddit_name_prefixed : String,
        var score : Int
)
