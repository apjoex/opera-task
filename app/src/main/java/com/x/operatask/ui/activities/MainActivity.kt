package com.x.operatask.ui.activities

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.httpGet
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.x.operatask.R
import com.x.operatask.adapters.PostAdapter
import com.x.operatask.models.Post
import com.x.operatask.utils.EndlessRecyclerViewScrollListener
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast
import org.json.JSONException

class MainActivity : AppCompatActivity() {

    private var mContext = this
    private var afterTag = ""
    private var redditPosts : MutableList<Post> = mutableListOf()
    private var additionalPosts : MutableList<Post> = mutableListOf()
    private lateinit var scrollListener : EndlessRecyclerViewScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val layoutManager = LinearLayoutManager(mContext)
        postList.layoutManager = layoutManager

        scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                loadMorePosts()
            }
        }

        postList.addOnScrollListener(scrollListener)

        fetchPosts()

    }

    private fun loadMorePosts() {

        val progressDialog = ProgressDialog.show(mContext, "Please wait", "Getting more posts...", false, false)

        val url = "${getString(R.string.baseUrl)}?after=$afterTag"
        url.httpGet().responseJson { _, _, result ->

            progressDialog.dismiss()

            val (res, err) = result

            if(err != null){
                toast(err.localizedMessage)
                return@responseJson
            }

            try {
                val baseObject = res?.obj()
                val postArray = baseObject?.optJSONObject("data")?.optJSONArray("children")
                afterTag = baseObject?.optJSONObject("data")?.optString("after") ?: ""

                val postType = object : TypeToken<MutableList<Post>>(){}.type
                additionalPosts = Gson().fromJson(postArray.toString(), postType)

                if (redditPosts.isNotEmpty()){
                    redditPosts.addAll(additionalPosts)
                    postList.adapter.notifyDataSetChanged()
                }

            }catch (e : JSONException){
                toast(e.localizedMessage)
            }
        }
    }

    private fun fetchPosts() {
        val url = "${getString(R.string.baseUrl)}?after=$afterTag"
        url.httpGet().responseJson { _, _, result ->
            val (res, err) = result

            if(err != null){
                toast(err.localizedMessage)
                return@responseJson
            }

            try {
                val baseObject = res?.obj()
                val postArray = baseObject?.optJSONObject("data")?.optJSONArray("children")
                afterTag = baseObject?.optJSONObject("data")?.optString("after") ?: ""

                val postType = object : TypeToken<MutableList<Post>>(){}.type
                redditPosts = Gson().fromJson(postArray.toString(), postType)

                if (redditPosts.isNotEmpty()){
                    loadingView.visibility = View.GONE

                    val adapter = PostAdapter(mContext, redditPosts)
                    postList.adapter = adapter

                }

            }catch (e : JSONException){
                toast(e.localizedMessage)
            }
        }
    }
}
